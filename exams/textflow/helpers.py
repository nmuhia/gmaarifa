# helper functions for texting
from django.template import Context, Template
from exams.models import Log, Exam, QUnit, Question, Response, Record
import sys
#import Talking
#from Talking import AfricasTalkingGateway 
import AfricasTalkingGateway as ATG
from django.shortcuts import render_to_response, render
  
from django.http import HttpResponse 
from django.template.context import RequestContext 


class TimesUpException(Exception):
  pass

def send_sms(client, user, message):
  # set up the "magic" variables for the rules.json
  ctx = {}
  ctx['first'] = user.firstname
  ctx['last'] = user.lastname
  if user.currentExam:
    ctx['exam'] = user.currentExam.title
    ctx['endTime'] = user.currentExam.endTime
  if user.currentQUnit:
    ctx['qunit'] = user.currentQUnit.title
    ctx['qunitDescription'] = user.currentQUnit.description
  if user.currentQ:
    ctx['question'] = user.currentQ.text
    ctx['choices'] = user.currentQ.choices
    ctx['answer'] = user.currentQ.correct
  t = Template(message)
  text = t.render(Context(ctx))
  
  # make a log, not sending just yet
  log = Log(source=client.shortCode, destination=user.phone, message=text, mock=client.mock)
  log.save()
  print log.source
  print log.destination
  print log.message

  # determine if message really has to be sent out
  if client.mock == False:
    username = 'gmaarifa'
    apikey   = '354e8eb6694f67dbb53133e4527370a8ac408b60b79de8f96c5e3bd6e77bf19d'
    gateway = ATG.AfricasTalkingGateway(username, apikey)
    to = destination
    message = message
    try:
      recipients = gateway.sendMessage(to, message)
      print to
      print message
      for x in recipients:
          print 'number=%s;status=%s;cost=%s' % (x['number'], x['status'], x['cost'])
      msg = 'Message sent successfully'
    except:
      e = sys.exc_info()[0]
      print "Error while connecting to the gateway: %s" % e
      msg = 'Error encountered, message not sent'
  else:
    username = 'gmaarifa'
    apikey   = '354e8eb6694f67dbb53133e4527370a8ac408b60b79de8f96c5e3bd6e77bf19d'
    gateway = ATG.AfricasTalkingGateway(username, apikey)
    to = log.destination
    message = log.message

    try:
      print 'message ' + to + message
      recipients = gateway.sendMessage(to, message)      
      print message
      for x in recipients:
          print 'number=%s;status=%s;cost=%s' % (x['number'], x['status'], x['cost'])
      msg = 'Message sent successfully'
    except:
      e = sys.exc_info()[0]
      print "Error while connecting to the gateway: %s" % e
      msg = 'Error encountered, message not sent'

def send_web(client, user, message):
  # set up the "magic" variables for the rules.json
  ctx = {}
  ctx['first'] = user.firstname
  ctx['last'] = user.lastname
  if user.currentExam:
    ctx['exam'] = user.currentExam.title
    ctx['endTime'] = user.currentExam.endTime
  if user.currentQUnit:
    ctx['qunit'] = user.currentQUnit.title
    ctx['qunitDescription'] = user.currentQUnit.description
  if user.currentQ:
    ctx['question'] = user.currentQ.text
    ctx['choices'] = user.currentQ.choices
    ctx['answer'] = user.currentQ.correct
  t = Template(message)
  text = t.render(Context(ctx))
  
  msg = {}
  # make a log, not sending just yet
  log = Log(source=client.shortCode, destination=user.phone, message=text, mock=client.mock)
  log.save()
  print log.source
  print log.destination
  print log.message 
  ctx['message'] = str(text)
  print 'ctx'
  print ctx
  #source = req.session['phone'] 
  #query = User.objects.filter(phone=source)
  #user = query[0]
  if user.state == 'done':
  	ctx = {}
  	ctx['state']='done'
  return ctx
  
        
def reply(client, user, message):
  # set up the "magic" variables for the rules.json
  ctx = {}
  ctx['first'] = user.firstname
  ctx['last'] = user.lastname
  if user.currentExam:  	
    ctx['exam'] = user.currentExam.title
    print user.currentExam.title
    print '1'
    ctx['endTime'] = user.currentExam.endTime
  if user.currentQUnit:  	
    ctx['qunit'] = user.currentQUnit.title
    print user.currentQUnit.title
    print '2'
    ctx['qunitDescription'] = user.currentQUnit.description
  if user.currentQ:  	
    ctx['question'] = user.currentQ.text
    print user.currentQ.text
    print '3'
    ctx['choices'] = user.currentQ.choices
    ctx['answer'] = user.currentQ.correct
  t = Template(message)
  text = t.render(Context(ctx))  
  ctx['msg'] = message
  
  print 'msssssssssssge'
  print message
  
  #print ctx['msg']
  
  
  # make a log, not sending just yet
  log = Log(source=client.shortCode, destination=user.phone, message=text, mock=client.mock)
  log.save()
  print log.source
  print log.destination
  print log.message
  print 'replying'
  print ctx
  return ctx

def webdirect(shortCode, phone, message):
  log = Log(source=shortCode, destination=phone, message=message)
  log.save()
  print message
  return message
  
def send_sms_direct(shortCode, phone, message):
  log = Log(source=shortCode, destination=phone, message=message)
  log.save()

def send_question(user, question):
  send_sms(user, question.text)
  send_sms(user, question.choices)


def completed(user, obj):
  res = False # assume not
  # first determine if it's a QUnit or an Exam
  if type(obj) == Exam:
    res = Record.objects.filter(kind='exam', completedId=obj.id, user=user)
  if type(obj) == QUnit:
    res = Record.objects.filter(kind='qunit', completedId=obj.id, user=user)
  if type(obj) == Question:
    res = Response.objects.filter(user=user, question=obj)
  return res
