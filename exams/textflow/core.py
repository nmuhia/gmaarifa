# TextFlow handles how a user goes throw the system

from clients.models import Client, Program
from exams.models import Exam, User

import helpers

class Machine:
  """
  The main thing.
  Handles rule processing logic for controlling the flow of a user.
  Basically a state machine for handling everything.
  """
  # rules is a list of dictionary rule objects
  def __init__(self, rules, handlers):
    self.handlers = handlers
    self.rules = {}
    # add all the rules
    for rule in rules:
      state = rule['state']
      self.rules[state] = rule
    # add this just because
    self.version = '0.1'
    
  def handleSMS(self, destination, source, message):
    # first and foremost, there must be a user object for a given source
    # this is a 'special case' if you will
    query = User.objects.filter(phone=source) # save this to prevent over hitting the db
    if not query:
      # the state is 'unknownPhone'
      rule = self.rules['unknownPhone']
      handler = self.handlers[rule['handler']]
      (res, user) = handler(destination, source, message) # the only handler w/ this signature
      # if successful, there should be a goTo in the rule to follow to next
      if res:
        if 'goTo' in rule:
          user.state = rule['goTo']
          user.save()
          # now go to it!
          return self.handleSMS(destination, source, message)
        else:
          # this is a failure case... idk what we should do right now...
          return
      else:
        # failed, send the error message
        errorMessage = rule['errorMessage']
        helpers.send_sms_direct(destination, source, errorMessage)
        return
    # ok, so there is a user
    user = query[0]
    client = user.program.client
    print client
    # find the rule and apply
    rule = self.rules[user.state]
    print rule
    # rules aren't required to have handlers, in which case they default to success
    res = True
    
    # if time's up, an exception will be thrown, catch it and goto 'timesUp' state
    try:
      if 'handler' in rule:
        handler = self.handlers[rule['handler']]
        res = handler(user, message)
        #print res
    except helpers.TimesUpException:
      user.state = "timesUp"
      user.save()
      print user
      return self.handleSMS(destination, source, message)  
    if res:
      print res
      # check if there is a successMessage and send it
      if 'successMessage' in rule:
        msg = rule['successMessage']
        helpers.send_sms(client, user, msg)
        print res
      # check if there is a goTo
      if 'goTo' in rule:
        user.state = rule['goTo']
        user.save()
        # go to it!
        return self.handleSMS(destination, source, message)
      # otherwise check if there is a stateTo
      if 'stateTo' in rule:
        user.state = rule['stateTo']
        user.save()
        # done
        return
    else:
      print res
      # handler failed, check for errorMessage
      if 'errorMessage' in rule:
        msg = rule['errorMessage']
        print msg
        print client
        print user
        
        helpers.send_sms(client, user, msg)
      # check if there is an errorTo
      if 'errorTo' in rule:
        user.state = rule['errorTo']
        user.save()
        # go to it!
        return self.handleSMS(destination, source, message)

  def handleWeb(self, destination, source, message):
    # first and foremost, there must be a user object for a given source
    # this is a 'special case' if you will
    query = User.objects.filter(phone=source) # save this to prevent over hitting the db
    if not query:
      # the state is 'unknownPhone'
      rule = self.rules['unknownPhone']
      handler = self.handlers[rule['handler']]
      (res, user) = handler(destination, source, message) # the only handler w/ this signature
      # if successful, there should be a goTo in the rule to follow to next
      if res:
        if 'goTo' in rule:
          user.state = rule['goTo']
          user.save()
          # now go to it!
          return self.handleWeb(destination, source, message)
        else:
          # this is a failure case... idk what we should do right now...
          return
      else:
        # failed, send the error message
        errorMessage = rule['errorMessage']
        helpers.send_sms_direct(destination, source, errorMessage)
        return
    # ok, so there is a user
    user = query[0]
    print 'gweB user'
    print user
    client = user.program.client
    print client
    # find the rule and apply
    rule = self.rules[user.state]
    print rule
    # rules aren't required to have handlers, in which case they default to success
    res = True
    
    # if time's up, an exception will be thrown, catch it and goto 'timesUp' state
    try:
      if 'handler' in rule:
        handler = self.handlers[rule['handler']]
        res = handler(user, message)
        #print res
    except helpers.TimesUpException:
      user.state = "timesUp"
      user.save()
      print user
      return self.handleWeb(destination, source, message)  
    if res:
      print res
      result = ''
      # check if there is a successMessage and send it
      if 'successMessage' in rule:
        msg = rule['successMessage']
        result = helpers.send_web(client, user, msg)
        print res
        print 'result'
        print result
      # check if there is a goTo
      if 'goTo' in rule:
        user.state = rule['goTo']
        user.save()
        print 'goTo result'
        print result
        ctx = {}
        ctx['verdict'] = result['message']
        print ctx
        result = ctx
        # go to it!
        #return self.handleWeb(destination, source, message)     
        
       # otherwise check if there is a stateTo
      if 'stateTo' in rule:
        user.state = rule['stateTo']
        user.save()
      if result != '':
		    print result      
		    return result     
      print 'result2'
      print result      
      return result
    else:
      print res
      result = ''
      # handler failed, check for errorMessage
      if 'errorMessage' in rule:
        msg = rule['errorMessage']
        print msg
        print client
        print user        
        result = helpers.send_web(client, user, msg)
        print 'result'
        print result
      # check if there is an errorTo
      if 'errorTo' in rule:
        user.state = rule['errorTo']
        user.save()
        print 'goTo result'
        print result
        ctx = {}
        ctx['verdict'] = result['message']
        print ctx
        result = ctx
        # go to it!
        #return self.handleWeb(destination, source, message)
      print 'result2'
      print result
      return result
      
        
  def handleWeb2(self, destination, source, message):
    # first and foremost, there must be a user object for a given source
    # this is a 'special case' if you will 
    print 'handleweb'
    print source
    query = User.objects.filter(phone=source) # save this to prevent over hitting the db
    if not query:
      # the state is 'unknownPhone'
      rule = self.rules['unknownPhone']
      handler = self.handlers[rule['handler']]
      (res, user) = handler(destination, source, message) # the only handler w/ this signature
      # if successful, there should be a goTo in the rule to follow to next
      if res:
        if 'goTo' in rule:
          user.state = rule['goTo']
          user.save()
          # now go to it!
          return self.handleWeb(destination, source, message)
        else:
          # this is a failure case... idk what we should do right now...
          return
      else:
        # failed, send the error message
        errorMessage = rule['errorMessage']
        print 'send_sms_direct'
        return helpers.webdirect(destination, source, errorMessage)
        
    # ok, so there is a user
    user = query[0]
    client = user.program.client
    print client
    # find the rule and apply
    rule = self.rules[user.state]
    print rule
    # rules aren't required to have handlers, in which case they default to success
    res = True
    
    # if time's up, an exception will be thrown, catch it and goto 'timesUp' state
    try:
      if 'handler' in rule:
        handler = self.handlers[rule['handler']]
        res = handler(user, message)
        #print res
    except helpers.TimesUpException:
      user.state = "timesUp"
      user.save()
      print user
      return self.handleWeb(destination, source, message)  
    if res:
      print res
      # check if there is a successMessage and send it
      if 'successMessage' in rule:
        msg = rule['successMessage']
        #helpers.reply(client, user, msg)
        #print res
        return helpers.reply(client, user, msg)
      # check if there is a goTo
      if 'goTo' in rule:
        user.state = rule['goTo']
        user.save()
        # go to it!
        return self.handleWeb(destination, source, message)
      # otherwise check if there is a stateTo
      if 'stateTo' in rule:
        user.state = rule['stateTo']
        user.save()
        # done
        return
    else:
      print res
      # handler failed, check for errorMessage
      if 'errorMessage' in rule:
        msg = rule['errorMessage']
        print msg
        print client
        print user
        #helpers.reply(client, user, msg)
        return helpers.reply(client, user, msg)
      # check if there is an errorTo
      if 'errorTo' in rule:
        user.state = rule['errorTo']
        user.save()
        # go to it!
        return self.handleWeb(destination, source, message)
        
