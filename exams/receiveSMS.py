import sys
import AfricasTalkingGateway as ATG

username = 'vincentmaraba'
apikey   = '6b314ee795b183146710dcd45f2688ade2e1099a0b5c253c866788e5cc0e6ecc'

gateway = ATG.AfricasTalkingGateway(username, apikey)

try:
    lastReceivedId = 0;
    while True:
	messages = gateway.fetchMessages(lastReceivedId)
	for x in messages:
            print 'from=%s;to=%s;date=%s;text=%s;' % (x['from'], x['to'], x['date'], x['text'])
            lastReceivedId = x['id']
	if len(messages) == 0:
            break
except:
    e = sys.exc_info()[0]
    print "Error while connecting to the gateway: %s" % e
