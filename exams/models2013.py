from django.db import models
from django.contrib import admin
from django.forms import ModelForm
from clients.models import Program
#from exams.models import Exam, QUnit, Question

 

class Exam(models.Model):
  title = models.CharField(max_length=50)
  description = models.TextField(blank=True, null=True)
  program = models.ForeignKey('clients.Program', related_name='exams')
  startTime = models.DateTimeField()
  endTime = models.DateTimeField()  #DateField
  ready = models.BooleanField(default=False)
  
  def __unicode__(self):
    return self.title
  
  def text_count(self):
    # going to send "You're about to start {{self.title}}. You have until {{self.endTime}} to complete this."
    # then send all the qunits
    # then end it with "Congrats you finished {{self.title}}."
    count = 0
    firstSMS = "You're about to start "+self.title+". You have until "+unicode(self.endTime)+" to complete this."
    firstL = len(firstSMS)
    count += (firstL / 160) + (firstL % 160 != 0)
    # now add all the qunits
    count += sum([qunit.text_count() for qunit in self.qunits.all()])
    lastSMS = "Congrats you've finished "+self.title+"."
    lastL = len(lastSMS)
    count += (lastL / 160) + (lastL % 160 != 0)
    return count

class QUnit(models.Model):
  title = models.CharField(max_length=100) # used to label the unit
  description = models.TextField(blank=True, null=True) # optional desc of the unit
  exam = models.ForeignKey('Exam', related_name='qunits')
  order = models.IntegerField(blank=True, null=True)

  def __unicode__(self):
    return self.title
  
  def text_count(self):
    # going to send "You're starting {{self.title}}. {{self.description}}"
    # also going to send all the texts from self.questions
    # also going to send "you've completed this quiz unit!" at the end
    count = 0
    firstSMS = "You're starting "+self.title+". "+self.description
    firstL = len(firstSMS)
    count += (firstL / 160) + (firstL % 160 != 0)
    # need to add all the question texts
    count += sum([q.text_count() for q in self.questions.all()])
    lastL = len("You've completed this quiz unit!")
    count += (lastL / 160) + (lastL % 160 != 0)
    return count


class Content(models.Model):
  content = models.TextField(blank=True,)
  qunit = models.ForeignKey('QUnit', blank=True, related_name='content')
  
class Question(models.Model):
  content = models.ForeignKey('Content')
  text = models.TextField()
  choices = models.TextField()
  correct = models.CharField(max_length=23) # i.e. "a" or "23" if number question 
  qunit = models.ForeignKey('QUnit', related_name='questions')
  order = models.IntegerField(blank=True, null=True)
  
  def __unicode__(self):
    return "Question #"+str(self.id) #self.order
  
  def text_count(self):
    # number of texts depends on how the question is formatted for output
    # need to do "{{self.text}}? {{self.choices}}"
    # and need to add one for "that's right / wrong" response text
    count = 0
    firstSMS = self.text + "? "+self.choices
    firstL = len(firstSMS)
    count += (firstL / 160) + (firstL % 160 != 0) # how many texts we'll send
    count += 1 # going to send a "right or wrong" text in response
    return count
  

class Response(models.Model):
  user = models.ForeignKey('User')
  question = models.ForeignKey('Question')
  qunit = models.ForeignKey('QUnit')
  exam = models.ForeignKey('Exam')
  text = models.CharField(max_length=160) # SMS's are limited to 160 chars
  correct = models.BooleanField()

# useful in finding what exams / qunits the user has completed
class Record(models.Model):
  user = models.ForeignKey('User')
  kind = models.CharField(max_length=30) # either 'exam' or 'qunit'
  completedId = models.IntegerField()


class Log(models.Model):
  destination = models.CharField(max_length=30)
  source = models.CharField(max_length=30)
  message = models.CharField(max_length=160)
  mock = models.BooleanField(default=True)
  arrived = models.DateTimeField(auto_now_add=True)
  
class User(models.Model):
  firstname = models.CharField(max_length=50, blank=True, null=True)
  lastname = models.CharField(max_length=50, blank=True, null=True)
  bday = models.CharField(max_length=50, blank=True, null=True)
  #bday = models.DateField(blank=True, null=True)
  phone = models.CharField(max_length=23, blank=True, null=True)
  program = models.ForeignKey('clients.Program', related_name='users')
  
  # temp things used by TextFlow
  state = models.CharField(max_length=23, blank=True,)
  currentExam = models.ForeignKey('Exam', blank=True, null=True)
  currentQUnit = models.ForeignKey('QUnit', blank=True, null=True)
  currentQ = models.ForeignKey('Question', blank=True, null=True)
  
  # other functions
  #def __unicode__(self):
  #  return self.lastname+", "+self.firstname+" - "+self.phone
  
  #default: 
	 #state = bday 
	 
#modeling forms
class UserForm(ModelForm):
    class Meta:
        model = User 

