from exams.models import User, Response, Question, QUnit, Exam, Log
from django.contrib import admin

admin.site.register(User)
admin.site.register(Question)
admin.site.register(QUnit)
admin.site.register(Exam)
admin.site.register(Response)
admin.site.register(Log)
