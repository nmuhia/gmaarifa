# sets up the data so that we can quickly change the db schema
from clients.models import Client, Program
from exams.models import Exam, QUnit, Question

from datetime import datetime, timedelta

# make two clients

clientA = Client(name="First Client Co.",
                 shortCode="4004",
                 username="bob",
                 password="$5$rounds=80000$w3xiI7/SQwPE4eBA$hRxUo/ZuvF60O0jX0gECBa8W29wzDjKKIqfWrslT9y9",
                 smsUser='blah',
                 smsPass='blah')
clientA.save()

clientB = Client(name="Second Client Co.",
                 shortCode="1234",
                 username="joe",
                 password="$5$rounds=80000$bTf10FGiVMCGC5Dl$jw6.aHe5TxP05muw/ZtNiUIVW/F11YBTgkKr7UiAi77",
                 smsUser='bleh',
                 smsPass='bleh')
clientB.save()

# just make the clients, should be able to do everything from the site
exit()

# make their programs
mathProgram = Program(name="Math Class",
                      programCode="123",
                      client=clientA)
mathProgram.save()

scienceProgram = Program(name="Science Class",
                      programCode="234",
                      client=clientA)
scienceProgram.save()

philProgram = Program(name="Intro to Philosophy",
                      programCode="345",
                      client=clientB)
philProgram.save()

# make the exam

mathTest = Exam(title="First Math Test",
                description="This won't be too hard... lol",
                program=mathProgram,
                startTime=datetime.now(),
                endTime=datetime.now()+timedelta(days=10))
mathTest.save()

# and now the QUnits
arith = QUnit(title="Arithmetic",
              description="This unit tests basic arithmetic.",
              exam=mathTest,
              order=1)
arith.save()
                   
calc = QUnit(title="Calculus",
             description="This unit tests basic calculus skills.",
             exam=mathTest,
             order=2)
calc.save()

real = QUnit(title="Real Analysis",
             description="This unit covers some analysis.",
             exam=mathTest,
             order=3)
real.save()

# now to add questions
q1 = Question(text="What is 2+2?",
              choices="a) 2. b) 3. c) 4. d) 5.",
              correct="c",
              qunit=arith,
              order=1)
q1.save()

q2 = Question(text="What is 2*2?",
              choices="a) 1. b) 2. c) 3. d) 4.",
              correct="d",
              qunit=arith,
              order=2)
q2.save()
                            
q3 = Question(text="What is 2^2?",
              choices="a) 2. b) 4. c) 1. d) 3.",
              correct="b",
              qunit=arith,
              order=3)
q3.save()

q4 = Question(text="What is the derivative of x^2?",
              choices="a) 2. b) (1/3)x^3. c) 2x. d) 2x^3.",
              correct="c",
              qunit=calc,
              order=1)
q4.save()
              
q5 = Question(text="What is integral sin(x) dx?",
              choices="a) cos(x). b) cos(x) + C. c) -cos(x) + C. d) x sin(x).",
              correct="c",
              qunit=calc,
              order=2)
q5.save()
              
              
q6 = Question(text="What is true about a compact set in R^k?",
              choices="a) It contains some but not all of it's limit points. b) It's closed and bounded. c) It's open.",
              correct="b",
              qunit=real,
              order=1)
q6.save()

# ok now it's decent for testing purposes

# space