import sys
import AfricasTalkingGateway as ATG

username = 'vincentmaraba'
apikey   = '6b314ee795b183146710dcd45f2688ade2e1099a0b5c253c866788e5cc0e6ecc'

gateway = ATG.AfricasTalkingGateway(username, apikey)

to      = "+254722221992"
message = "I'm a lumberjack and it's ok, I sleep all night and I work all day"

try:
    recipients = gateway.sendMessage(to, message)
    for x in recipients:
        print 'number=%s;status=%s;cost=%s' % (x['number'], x['status'], x['cost'])
except:
    e = sys.exc_info()[0]
    print "Error while connecting to the gateway: %s" % e
