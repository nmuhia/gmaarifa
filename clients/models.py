from django.db import models


class Client(models.Model):
  name = models.CharField(max_length=50)
  shortCode = models.CharField(max_length=10, unique=True)
  username = models.CharField(max_length=30, unique=True)
  password = models.CharField(max_length=150) # big for big hashes
  # gotta keep a cleartext password for the SMS system...
  smsUser = models.CharField(max_length=30)
  smsPass = models.CharField(max_length=30)
  mock = models.BooleanField(default=True)
  
  def __unicode__(self):
    return self.name


class Program(models.Model):
  name = models.CharField(max_length=50)
  programCode = models.CharField(max_length=50)
  client = models.ForeignKey('Client', related_name='programs')
  
  def __unicode__(self):
    return self.name
    
  
