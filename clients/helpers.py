from django.shortcuts import redirect
from models import Client
from exams.models import User
# cool helpers

# Le cool decorator
def loggedIn(fn):
  def wrapped(*args, **kargs):
    # the request object is first
    req = args[0]
    if 'clientId' in req.session:
      # find clients by id
      clientId = req.session['clientId']
      try:
        client = Client.objects.get(pk=clientId)
        # now call the function with the new context
        return fn(client, *args, **kargs)
      except Client.DoesNotExist:
        #pass
        return wrapped
    # this far means not authenticated, redirect to login page
    return redirect('/login')
  return wrapped


# Le cool decorator
def loggedInUser(fn):
  def wrapped(*args, **kargs):
    # the request object is first
    req = args[0]
    if 'username' in req.session:
      # find users by username
      username = req.session['username']
      try:
        user= username
        # now call the function with the new context
        return fn(user, *args, **kargs)
      except User.DoesNotExist:
        #pass
        return wrapped
    # this far means not authenticated, redirect to login page
    return redirect('/logoutgweb')
  return wrapped
