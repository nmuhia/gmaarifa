from clients.models import Client, Program
from django.contrib import admin


admin.site.register(Client)
admin.site.register(Program)
