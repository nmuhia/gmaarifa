from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect

from clients.helpers import loggedIn
from clients.models import Client, Program

from passlib.hash import sha256_crypt
from django.template import RequestContext

# Le cool decorator thing
# fn :: HttpRequest -> HttpResponse
def onlyAdmin(fn):
  def wrapped(req, *args, **kargs):
    if req.user.is_authenticated():
      ctx = {}
      ctx.update(csrf(req))
      ctx['user'] = req.user
      return fn(req, ctx, *args, **kargs)
    else:
      return redirect('/')
  return wrapped

def programAuth(fn):
  def wrapped(client, req, *args, **kargs):
    program = Program.objects.get(pk=int(kargs['id']))
    auth = program.client
    if client == auth:
      return fn(client, req, *args, **kargs)
    else:
      return redirect('/')
  return wrapped

# make a client creation page
@onlyAdmin
def new_client(req, ctx):
  # present the form for creating a new client
  return render_to_response('clients/new_client.html', ctx)

@onlyAdmin
def clients(req, ctx):
  ctx['clients'] = Client.objects.all()
  return render_to_response('clients/clients.html', ctx)

@loggedIn
@programAuth
def viewProgram(client, req, id):
  program = Program.objects.get(pk=int(id))
  ctx = {}
  ctx['program'] = program
  ctx['client'] = client
  return render_to_response('clients/viewPrograms.html', ctx)

#@loggedIn
#@programAuth
@csrf_exempt
def viewPrograms(req):
	myId = req.POST['course']	
	#exams = Exam.objects.all()
	program = Program.objects.get(pk=int(myId))
	ctx = {}
	ctx['program'] = program
	programs = Program.objects.all()
	return render_to_response('exams/viewExamEdited.html', locals())

@loggedIn
def programForm(client, req, mode="new", id=None):
  ctx = {}
  ctx.update(csrf(req))
  ctx['client'] = client
  if mode == "edit":
    # edit mode
    program = Program.objects.get(pk=int(id))  
    ctx['program'] = program
  return render_to_response('clients/programForm.html', ctx)

@loggedIn
def createProgram(client, req):
  name = req.POST['name']
  code = req.POST['code']
  program = Program(name=name, programCode=code, client=client)
  program.save()
  
  return redirect('/home')

@loggedIn
@programAuth
def updateProgram(client, req, id):
  program = Program.objects.get(pk=int(id))
  program.name = req.POST['name']
  program.code = req.POST['code']
  program.save()
  
  return redirect("/programs/"+str(program.id))

def adminLogin(req):
  ctx = {}
  ctx.update(csrf(req))
  if 'username' in req.POST:
    # logging in
    username = req.POST['username']
    password = req.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
      login(req, user)
      return redirect('/admin')
  
  return render_to_response('clients/adminLogin.html', ctx)

@onlyAdmin
def adminLogout(req, ctx):
  logout(req)
  return redirect('/')

@onlyAdmin
def adminView(req, ctx):
  # display a list of clients
  ctx['clients'] = Client.objects.all()
  return render_to_response('clients/adminView.html', ctx)

@onlyAdmin
def viewAs(req, ctx, id):
  # basically login w/ this client id then redirect to "/home"
  req.session['clientId'] = id
  return redirect('/home')

@onlyAdmin
def clientForm(req, ctx, mode="new", id=None):
  if mode == "edit":
    ctx['client'] = Client.objects.get(pk=int(id))
  
  return render_to_response('clients/clientForm.html', ctx)

@onlyAdmin
def createClient(req, ctx):
  name = req.POST['name']
  shortCode = req.POST['code']
  username = req.POST['username']
  password = req.POST['password']
  smsUser = req.POST['smsUser']
  smsPass = req.POST['smsPass']
  mock = req.POST['mock'] == 'yes'
  # hash the password
  hashed = sha256_crypt.encrypt(password)
  # create the client
  client = Client(name=name, shortCode=shortCode, username=username,
                  password=hashed, smsUser=smsUser, smsPass=smsPass,
                  mock=mock)
  client.save()
  return redirect('/admin')

@onlyAdmin
def updateClient(req, ctx, id):
  client = Client.objects.get(pk=int(id))
  
  client.name = req.POST['name']
  client.shortCode = req.POST['code']
  client.username = req.POST['username']
  client.password = req.POST['password']
  client.smsUser = req.POST['smsUser']
  client.smsPass = req.POST['smsPass']
  client.mock = req.POST['mock'] == 'yes'
  # hash the password
  client.password = sha256_crypt.encrypt(client.password)
  
  client.save()
  return redirect('/admin')

# space!
