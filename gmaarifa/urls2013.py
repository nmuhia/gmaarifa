#from django.conf.urls import patterns, include, url
from django.conf.urls.defaults import *
from django.conf.urls import *
from pages import views

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',    
    url(r'^$', 'pages.views.welcome'), # done
    url(r'^docs/?$', 'pages.views.docs'),
    url(r'^home/?$', 'pages.views.home'),
    url(r'^courses/?$', 'pages.views.courses'),
		#url(r'^program/$', 'clients.views.viewPrograms'),
		#url(r'^selectCourse/?$', 'pages.views.selectCourse'),
    url(r'^programs/new/?$', 'clients.views.programForm'),
    url(r'^programs/create/?$', 'clients.views.createProgram'),
    url(r'^programs/(?P<id>\d+)/?$', 'clients.views.viewProgram'),		
    url(r'^programs/(?P<id>\d+)/edit/?$', 'clients.views.programForm', {"mode": "edit"}),
    url(r'^programs/(?P<id>\d+)/update/?$', 'clients.views.updateProgram'),    
    
    url(r'^programs/(?P<id>\d+)/exam/?$', 'exams.views.examForm'),
    url(r'^exams/create/?$', 'exams.views.createExam'),
    url(r'^exams/(?P<id>\d+)/?$', 'exams.views.viewExam'),   
    url(r'^exams/(?P<id>\d+)/edit/?$', 'exams.views.examForm', {"mode": "edit"}),
    url(r'^exams/(?P<id>\d+)/update/?$', 'exams.views.updateExam'),
    url(r'^exams/(?P<id>\d+)/delete/?$', 'exams.views.deleteExam'),
    url(r'^exams/performance/?$', 'exams.views.performance'),
    
    url(r'^exams/(?P<id>\d+)/qunit/?$', 'exams.views.qUnitForm'),
    url(r'^exams/qunits/create/?$', 'exams.views.createQUnit'),
    url(r'^exams/qunits/(?P<id>\d+)/?$', 'exams.views.viewQUnit'),
    url(r'^exams/qunits/(?P<id>\d+)/edit/?$', 'exams.views.qUnitForm', {"mode": "edit"}),
    url(r'^exams/qunits/(?P<id>\d+)/update/?$', 'exams.views.updateQUnit'),
    url(r'^exams/qunits/(?P<id>\d+)/delete/?$', 'exams.views.deleteQUnit'),
    
    url(r'^exams/questions/create/?$', 'exams.views.createQ'),
    url(r'^exams/questions/(?P<id>\d+)/?$', 'exams.views.viewQ'),
    url(r'^exams/questions/(?P<id>\d+)/edit/?$', 'exams.views.qForm'),
    url(r'^exams/questions/(?P<id>\d+)/update/?$', 'exams.views.updateQ'),
    url(r'^exams/questions/(?P<id>\d+)/delete/?$', 'exams.views.deleteQ'),
    
    url(r'^users/(?P<id>\d+)/?$', 'exams.views.viewUser'),
    
    url(r'^addUser/?$', 'exams.views.uploadCSV'), #addUser
    url(r'^oneUser/?$', 'exams.views.addUser'),
    url(r'^profile/?$', 'exams.views.viewAllUsers'),
    url(r'^reports/(?P<id>\d+)/?$', 'exams.views.reports'),  
    url(r'^reports/?$', 'pages.views.reports'),
    url(r'^viewreport/$', 'exams.views.viewreport'),
    url(r'^sms/?$', 'pages.views.sms'), #console
    url(r'^console/?$', 'pages.views.console'),
    url(r'^login/?$', 'pages.views.login'),
    url(r'^logout/?$', 'pages.views.logout'),

    #added April by Vincent
    url(r'^clients/?$', 'clients.views.clients'),
    url(r'^send/?$', 'exams.sendSMS.send'),
    url(r'^send1SMS/?$', 'exams.sendSMS.send'),

    url(r'^receiveSMS/?$', 'exams.views.receiveSMS'),    
    url(r'^receiveConsole/?$', 'exams.views.receiveConsole'),
    url(r'^smsLog/?$', 'exams.views.smsLog'),
    
    url(r'^admin/login', 'clients.views.adminLogin'),
    url(r'^admin/logout', 'clients.views.adminLogout'),
    url(r'^admin/?$', 'clients.views.adminView'),
    url(r'^view-as/(?P<id>\d+)/?$', 'clients.views.viewAs'),
    url(r'^admin/clients/new/?$', 'clients.views.clientForm'),
    url(r'^admin/clients/(?P<id>\d+)/edit/?$', 'clients.views.clientForm', {"mode":"edit"}),
    url(r'^admin/clients/create/?$', 'clients.views.createClient'),
    url(r'^admin/clients/(?P<id>\d+)/update/?$', 'clients.views.updateClient'),
    
    # Examples:
    # url(r'^$', 'gmaarifa.views.home', name='home'),
    # url(r'^gmaarifa/', include('gmaarifa.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^django-admin/', include(admin.site.urls)),
)
