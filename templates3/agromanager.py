from osv import osv, fields
import time
from tools.translate import _
import netsvc
import datetime
from datetime import date

class agromanager_diseases(osv.osv):
	_name='agromanager.diseases'
	_description='Diseases Module'
	
	_columns={
		'name':fields.char('Name', size=60, required=True),
		'symptom':fields.char('Major Symptom', size=255, required=False),
	}
agromanager_diseases()

class agromanager_methods(osv.osv):
	_name='agromanager.methods'
	_description='methods Module'
	
	_columns={
		'crop':fields.char('Crop', size=60, required=True),
		'method':fields.char('Farming Method Symptom', size=255, required=False),
	}
agromanager_methods()

class agromanager_quality_checks(osv.osv):
	_name='agromanager.quality.checks'
	_description='Quality Checks Module'
	
	_columns={
		'name':fields.char('Quality Check', size=255, required=True),
	}
agromanager_quality_checks()

class agromanager_pesticides(osv.osv):
	_name='agromanager.pesticides'
	_description='Chemicals/Pesticides Module'
	
	_columns={
		'name':fields.char('Name', size=60, required=True),
		'manufacturer':fields.char('Trade Name', size=60, required=True),
		'target':fields.char('Target', size=60, required=False),
		'use_rate':fields.char('Max Rate of Use/Ha', size=60, required=False),
		'rate':fields.char('Max Rate per 100L', size=60, required=False),
		'restrictions':fields.char('Use Restrictions', size=60, required=False),
		'water_volume':fields.char('Std Water Volume/Ha', size=60, required=False),
		'harvest_interval':fields.char('Pre-Harvest Interval', size=60, required=False),
		'who':fields.char('WHO Class', size=60, required=False),
		're_entry':fields.char('Re-Entry Interval', size=60, required=False),
		'eu_uk':fields.char('EU/UK MRL', size=60, required=False),
		'pesticide_cleared':fields.char('Pesticide Cleared in Country of Origin', size=60, required=False),
		'reasons':fields.char('Reasons for Use', size=60, required=False),
	}
agromanager_pesticides()

class agromanager_blocks(osv.osv):
	_name='agromanager.blocks'
	_description='Farm Blocks'
	
	_columns={
		'name':fields.char('Name', size=60, required=True),
		'block_id':fields.char('Block Id', size=60, required=False),
	}
agromanager_blocks()

class agromanager_pests(osv.osv):
	_name='agromanager.pests'
	_description='Pests Module'
	
	_columns={
		'name':fields.char('Name', size=60, required=True),
	}
agromanager_pests()

class agromanager_insects(osv.osv):
	_name='agromanager.insects'
	_description='Insects Module'
	
	_columns={
		'name':fields.char('Name', size=60, required=True),
	}
agromanager_insects()

class agromanager_fertilizer(osv.osv):
	_name='agromanager.fertilizer'
	_description='Fertilizer Module'
	
	_columns={
		'name':fields.char('Name', size=60, required=True),
		'type':fields.char('Type', size=60, required=True),
	}
agromanager_fertilizer()

class agromanager_scouting(osv.osv):
	_name='agromanager.scouting'
	_description='Scouting Module'
	
	_columns={
		'scouting_date':fields.date('Scouting Date', required=False),
		#'prodlot_id': fields.many2one('stock.production.lot', 'Production Lot', help="Production lot is used to put a serial number on the production", select=True, required=False),
		'partner_id':fields.many2one('res.partner', 'Farmer', required=True),
		'agromanager_blocks_id':fields.many2one('agromanager.blocks', 'Block', required=False),
		'agromanager_insects_id':fields.many2one('agromanager.insects', 'Beneficial Insects', required=False),
		'agromanager_diseases_id':fields.many2one('agromanager.diseases', 'Diseases', required=False),
		'agromanager_pests_id':fields.many2one('agromanager.pests', 'Pests', required=False),
		'agromanager_pesticides_id':fields.many2one('agromanager.pesticides', 'Pesticide', required=False),
		'pesticide_qty':fields.float('Pesticide Amount(ml)', size=60, required=False),
		'water':fields.selection([('no','No'), ('yes','Yes')], 'Needs Water'),
		'weeding':fields.selection([('no','No'), ('yes','Yes')], 'Needs Weeding'),
		'fertilizer_id':fields.many2one('agromanager.fertilizer', 'Fertilizers', required=False),
		'planting_id':fields.many2one('agromanager.seed.planting', 'Seed Planting', required=True),
		'name': fields.char('Scouting Reference', size=64, required=True,
            readonly=True),
		'prodlot_id': fields.many2one('stock.production.lot', 'Production Lot')
	}

	_defaults = {
        'scouting_date': fields.date.context_today,
		'name': lambda obj, cr, uid, context: obj.pool.get('ir.sequence').get(cr, uid, 'agromanager.scouting'),
	}
agromanager_scouting()


class agromanager_quality_check_details(osv.osv):
	_name='agromanager.quality.check.details'
	_description='Quality Checks Detail Module'
	
	_columns={
		#'purchase_order_id':fields.one2many('purchase.order', 'id', 'Purchase Order',required=False),
		'quality_checks_id':fields.many2one('agromanager.quality.checks', 'Quality Checks', required=False),
		'purchase_order_id':fields.many2one('purchase.order','Purchase Order', ondelete='set null',required=False),
		'rejection_reason':fields.char('Rejection Reason', size=160,required=False),
	}
agromanager_quality_check_details()

class agromanager_seed_distribution(osv.osv):
	_name='agromanager.seed.distribution'
	_description='Seed Distribution Module'
	
	
	'''def _dist_id(self,cr,uid,ids,field,arg,context=None):
		i = 1
		#my_id = partner_id
		today = '';	
		y = str(i)	
		dist = 'A' + y
		return dist'''
	
	_columns={
		'name': fields.char('Distribution Reference', size=64, required=True,
            readonly=True),
		'partner_id':fields.many2one('res.partner', 'Farmer', required=True),
		'seed':fields.many2one('product.product', 'Seed Crop', required=True),
		'variety_id':fields.many2one('agromanager.crop.varieties', 'Variety', required=False),
		'weight':fields.float('Weight'),
		'planting_date':fields.date('Expected Harvest Date', required=True),
		'seed_lot_no':fields.char('Seed Lot No', size=60, required=False),
		
	}
	
	_defaults = {
       #'name': fields.date.context_today,
	   'name': lambda obj, cr, uid, context: obj.pool.get('ir.sequence').get(cr, uid, 'agromanager.seed.distribution'),
		
	}

	'''def copy(self, cr, uid, id, default=None, context=None):
          if not default:
               default = {}
          default.update({
              'name': self.pool.get('ir.sequence').get(cr, uid, 'agromanager.seed.distribution'),
          })
          return super(agromanager_seed_distribution, self).copy(cr, uid, id, default, context=context)'''

	'''def copy(self, cr, uid, id, default=None,context={}):
		name=self.seq_get(cr, uid)
       	if not default:
			default = {}
       	#default.update({'dist_no': name,})
       		return super(osv.osv, self).copy(cr, uid, id, default, context)
	
	def seq_get(self, cr, uid):
		pool_seq=self.pool.get('ir.sequence')
       	cr.execute("select id,number_next,number_increment,prefix,suffix,padding from ir_sequence where code='number' and active=True")
       	res = cr.dictfetchone()
       	if res:
			if res['number_next']:
				return pool_seq._process(res['prefix']) + '%%0%sd' % res['padding'] % res['number_next'] + pool_seq._process(res['suffix'])
        else:
        	return pool_seq._process(res['prefix']) + pool_seq._process(res['suffix'])
       	return False
      
	def create(self, cr, user, vals, context=None):
		name=self.pool.get('ir.sequence').get(cr, user, 'number')
       	return super(number,self).create(cr, user, vals, context)'''

agromanager_seed_distribution()

class agromanager_seed_planting(osv.osv):
	_name='agromanager.seed.planting'
	_description='Seed Planting Module'

	def _sel_func(self, cr, uid, context=None):
		obj = self.pool.get('agromanager.seed.distribution')
		ids = obj.search(cr, uid, [])
		res = obj.read(cr, uid, ids, ['name0', 'id'], context)
		res = [(r['id'], r['name']) for r in res]
		return res

	_columns={
		'name': fields.char('Planting Reference', size=64, required=True,
            readonly=True),
		'partner_id':fields.many2one('res.partner', 'Farmer', required=True),
		'planted':fields.selection([('yes','Yes'), ('no','No')], 'Planted', required=True),
		'product_id':fields.many2one('product.product', 'Crop', required=True),
		'variety_id':fields.many2one('agromanager.crop.varieties', 'Variety', required=True),
		'block':fields.selection([('1','Block 1'), ('2','Block 2'), ('3','Block 3'), ('4','Block 4')],'Block', required=False),
		'planting_date':fields.date('Planting Date', required=True),
		#'planting_id':fields.many2one('agromanager.planting.method', 'Planting Method', required=True),
		'planting_rate':fields.integer('Planting Rate', required=False),
		'seed_quantity':fields.float('Seed Quantity', required=False),
		'distribution_id':fields.many2one('agromanager.seed.distribution', 'Seed Distribution', required=False),
		#'my_field': fields.many2one('agromanager.seed.distribution','Title'),
	}
	_defaults = {
        'planting_date': fields.date.context_today,
		'name': lambda obj, cr, uid, context: obj.pool.get('ir.sequence').get(cr, uid, 'agromanager.seed.planting'),
	}
agromanager_seed_planting()

class agromanager_analysis(osv.osv):
	_name='agromanager.analysis'
	_description='Performance Analysis'
	
	_columns={
		'name':fields.many2one('res.partner', 'Farmer', required=True),
		'seed_quantity':fields.float('Seeds Planted (Kg)', required=False),
		'harvest':fields.float('Harvest (Kg)', required=False),
		'day':fields.date('Date', required=True),
	}
	_defaults = {
        'day': fields.date.context_today,
	}
agromanager_analysis()

class agromanager_crop_varieties(osv.osv):
	_name='agromanager.crop.varieties'
	_description='Crop Varieties Module'
	
	_columns={
		'product_id':fields.many2one('product.product', 'Crop', required=True),
		'name':fields.char('Variety Name', size=60, required=True),
	}
agromanager_crop_varieties()

class agromanager_methods(osv.osv):
	_name='agromanager.methods'
	_description='Planting Method Module'
	
	_columns={
		'product_id':fields.many2one('product.product', 'Crop', required=False),
		'name':fields.char('Planting Method', size=60, required=False),
	}
agromanager_methods()

class agromanager_planting_method(osv.osv):
	_name='agromanager.planting.method'
	_description='Planting Methods Module'
	
	_columns={
		'product_id':fields.many2one('product.product', 'Crop', required=True),
		'name':fields.char('Planting Method', size=60, required=True),
	}
agromanager_planting_method()

class hillside_messages(osv.osv):
    """
    Message from one user to another within a hillside
    """
    _name = 'hillside.messages'
    logger = netsvc.Logger()

    _columns = {
        'create_date': fields.datetime('Creation Date', readonly=True),
        'from_id': fields.many2one('res.users', 'From', required=True, ondelete="CASCADE"),
        'to_id': fields.many2one('res.users', 'To', ondelete="CASCADE", help="Keep this empty to broadcast the message."),
        'message': fields.text('Message', required=True),
    }
    
    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        # return messages by current user, for current user or for all users
        # return all messages if current user is administrator  
        if uid != 1:
            args.extend(['|',('from_id', 'in', [uid,]),('to_id', 'in', [uid, False])])
        return super(hillside_messages, self).search(cr, uid, args, offset, limit,
                order, context=context, count=count)
        
    _defaults = {
        'from_id': lambda self, cr, uid, context: uid,
    }
hillside_messages()

'''
class purchase_order(osv.osv):
	_inherit = 'purchase.order'
	_columns = {
		'agromanager_id':fields.many2one('agromanager.seed.planting', 'Manager'),
		'quality_check':fields.many2one('agromanager.quality.checks'),
		'packability': fields.char('Packability (%)', size=64, ),  
	}
purchase_order()
'''





